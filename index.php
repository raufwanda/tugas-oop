<?php

    require_once('animal.php');
    require_once('frog.php');
    require_once('ape.php');
    $animal = new Animal("shaun");
    echo "Nama = ".$animal->name ."<br>";
    echo "Legs = ".$animal->legs ."<br>";
    echo "cold blooded = ". $animal->coldblooded ."<br> <br>";

    $frog = new Frog("buduk");
    echo "Nama = ".$frog->name ."<br>";
    echo "Legs = ".$frog->legs ."<br>";
    echo "cold blooded = ". $frog->coldblooded ."<br>";
    echo "Jump = ". $frog->Jump ."<br> <br>";

    $ape = new Ape("kera sakti");
    echo "Nama = ".$ape->name ."<br>";
    echo "Legs = ".$ape->legs ."<br>";
    echo "cold blooded = ". $ape->coldblooded ."<br>";
    echo "Jump = ". $ape->Yell ."<br>";

?>